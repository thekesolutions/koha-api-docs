# Build the HTML files
FROM node:12-alpine as build

LABEL org.opencontainers.image.authors="Tomás Cohen Arazi <tomascohen@theke.io>"
LABEL description="Docker image for hosting the Koha REST API documentation"

# Add git and redoc-cli
RUN apk --no-cache add git
# Latest is 0.11.4, but breaks with our spec
RUN npm install -g redoc-cli@0.10.4

RUN mkdir /build
RUN mkdir /workdir

COPY templates /templates
COPY files/favicon.ico /build

WORKDIR /workdir

RUN git ls-remote --refs --tags \
      --sort=version:refname https://git.koha-community.org/Koha-community/Koha.git \
      | cut -f2 | cut -d'/' -f 3 | grep "^v[[:digit:]][[:digit:]]" > ./tagslist \
      && cat ./tagslist|awk '{gsub(/v/,""); FS="."; if($1$2 >= 1911) print $1"."$2}'|uniq > ./brancheslist \
      && export LAST_BRANCH=$(tail -n 1 ./brancheslist) \
      && for branch in $(cat ./brancheslist); do \
            echo "Processing ${branch}"; \
            export TEMPLATE_OPS=""; \
            export COUNT=$(wc -l ./brancheslist|cut -d" " -f1); \
            for menu_branch in $(cat ./brancheslist); do \
                  export TEMPLATE_OPS=" --templateOptions.menu.m${COUNT}.name=\"${menu_branch}\" --templateOptions.menu.m${COUNT}.active="$(if [[ "$branch" = "${menu_branch}" ]]; then echo -n "1"; else echo -n "0"; fi)$TEMPLATE_OPS; \
                  export COUNT=$((COUNT-1)); \
            done; \
            export TEMPLATE_OPS=" --templateOptions.menu.m0.name=Unstable --templateOptions.menu.m0.active=0"$TEMPLATE_OPS; \
            echo $TEMPLATE_OPS; \
            export KOHA_TAG=$(cat ./tagslist|grep ${branch}|tail -n 1); \
            git clone --depth 1 https://git.koha-community.org/Koha-community/Koha.git --branch ${KOHA_TAG} --single-branch ${branch}; \
            export SWAGGER_FILE="${branch}/api/v1/swagger/swagger.yaml"; \
            if [[ -f "${branch}/api/v1/swagger/swagger.json" ]]; then \
                  export SWAGGER_FILE="${branch}/api/v1/swagger/swagger.json"; \
            fi; \
            redoc-cli bundle \
                  --cdn \
                  --output /build/${branch}.html \
                  -t /templates/main.hbs \
                  $TEMPLATE_OPS \
                  $SWAGGER_FILE; \
         done \
      && echo "Processing Unsable" \
      && export TEMPLATE_OPS=""; \
            export COUNT=$(wc -l ./brancheslist|cut -d" " -f1); \
            for menu_branch in $(cat ./brancheslist); do \
                  export TEMPLATE_OPS=" --templateOptions.menu.m${COUNT}.name=\"${menu_branch}\" --templateOptions.menu.m${COUNT}.active=0"$TEMPLATE_OPS; \
                  export COUNT=$((COUNT-1)); \
            done; \
            export TEMPLATE_OPS=" --templateOptions.menu.m0.name=Unstable --templateOptions.menu.m0.active=1"$TEMPLATE_OPS \
      && git clone --depth 1 https://git.koha-community.org/Koha-community/Koha.git --branch master --single-branch unstable \
      && export SWAGGER_FILE="unstable/api/v1/swagger/swagger.yaml"; \
            if [[ -f "unstable/api/v1/swagger/swagger.json" ]]; then \
                  export SWAGGER_FILE="unstable/api/v1/swagger/swagger.json"; \
            fi \
      && redoc-cli bundle \
            --cdn \
            --output /build/Unstable.html \
            -t /templates/main.hbs \
            $TEMPLATE_OPS \
            $SWAGGER_FILE \
      && cd /build ; \
            ln -s ${LAST_BRANCH}.html index.html

# Get rid of the previous layers, just keep the /build volume
FROM nginx

COPY --from=build /build /usr/share/nginx/html
